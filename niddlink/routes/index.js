var express = require('express');
var router = express.Router();
var http = require('http');



var availableUsers = new Array();


//====================CHAT======================================


httpServer = http.createServer(function(req,res) {
  console.log('une nouvelle connexion');
  res.end('Bienvenue');
});

//Port d'écoute du chat
httpServer.listen(3001);

var io = require('socket.io').listen(httpServer);

var chatSessions = new Array();
chatSessions.push({"id":0, "targetUser":null, "initialUser":null});



io.sockets.on('connection',function(socket) {

	console.log("socketFTW");

	//Client demande à entamer une conversation
	socket.on('newRequest',function(data){
		var userId = JSON.parse(data).userId;
		var initialUserId = JSON.parse(data).initialUserId;
		
		//Vérifier si les utilisateurs concernés par la requête d'ouverture de salon 
		//	font partie de la liste des utilisateurs connectés
		var a = -1;
		var b = -1;
		if(userId != 666)
		{
			for(var i in availableUsers)
			{
				if(availableUsers[i].id == userId)
				{
					a = i;
				}
				if(availableUsers[i].id == initialUserId)
				{
					b = i;
				}
			}
		}

		//S'ils le sont, passer leur statut sur occupé (en conversation)
		if(a != -1)
			availableUsers[a].busy = true;
		if(b != -1)
			availableUsers[b].busy = true;

		
	    console.log("Nouvelle requête à destination de : "+userId);

	    //ce if est une protection brevetée anti-bug : le client doit faire une requête newRequest lorsqu'on lui indique
	    //	que quelqu'un à ouvert une room pour chatter avec lui, sinon il ne peut pas répondre (pour une raison inconnue
	    //		et ennuyante). On effectue donc cette requête avec un id bidon qui permet de ne pas faire trop de bêtises
		//			côté serveur.
	    if(JSON.parse(data).userId != 666)
	    {
	    	//ajouter un nouveau salon au tableau puis émettre sur le socket pour indiquer le numéro de room assigné
	    	chatSessions.push({"id":chatSessions[chatSessions.length-1].id+1, "targetUser":userId, "initialUser": initialUserId});
	    	io.sockets.emit('sessionRoomAssigned_'+initialUserId, chatSessions[chatSessions.length-1].id);
		}

	    //Pour chaque salon créé, créer un listener de nouveaux messages
		for(var id in chatSessions)
		{
			console.log("listener ajouté : clientSendNewMessage_Room"+chatSessions[id].id);
			socket.on('clientSendNewMessage_Room'+chatSessions[id].id, function(data){
				var message = JSON.parse(data).message;
				var sender = JSON.parse(data).sender;
				io.sockets.emit('serverSendNewMessage_Room'+chatSessions[id].id, JSON.stringify({"message": message, "sender": sender}));
				console.log("====>message to "+chatSessions[id].id+" : "+message);
			});

			//Listener de requête de fermeture de la room
			socket.on('destroySessionRoom_'+chatSessions[id].id,function(data){
				//Message magique signifiant à l'autre utilisateur de quitter la room
				io.sockets.emit('serverSendNewMessage_Room'+data.sessionRoom, JSON.stringify({"message": "@@closed_room@@", "sender": null}));
				console.log("session room destroyed : "+chatSessions[id].id)

				chatSessions[id].initialUser = null;
				chatSessions[id].targetUser = null;

				//Rendre les utilisateurs disponible à nouveau
				for(var i in availableUsers)
				{
					if(availableUsers[i].id == data.userId || availableUsers[i].id == data.currentUserId)
					{
						console.log("user is not busy anymore : "+data.userId);
						availableUsers[i].busy = false;
					}
				}
				
			});
		}
	});
});


router.post('/stillAlive', function(req, res, next) {
	//empêcher qu'un utilisateur soit inscrit deux fois en même temps
	var aa = false;
	for(var key in availableUsers)
	{
		
		if(JSON.stringify(availableUsers[key].id) === JSON.stringify(req.session.currentUser))
		{
			aa = true;
		}
	}
	if(aa === false)
	{
		availableUsers.push({"id": req.session.currentUser, "busy": false});
	}

	var sent = false;
	//Mise en relation pour le chat : permet à un utilisateur d'être mis au courant qu'un salon s'est ouvert pour lui
	for(var id in chatSessions)
	{
		var reverseId = chatSessions.length-id-1;
		//si le client est cible d'une nouvelle conversation, le prévenir
		if(chatSessions[reverseId].targetUser === req.session.currentUser)
		{
			sent = true;
			res.end(JSON.stringify({"chatSession": chatSessions[reverseId].id, "idPerson": chatSessions[reverseId].initialUser}));
		}
	}

	//Si rien à signaler, renvoyer l'acquittement simple.
	res.end("ok");
});



// ===================== API REST ========================


/* GET home page. */
router.get('/', function(req, res, next) {
	console.log(JSON.stringify(availableUsers));
	availableUsers.splice(availableUsers.indexOf(req.session.currentUser), 1);
	console.log(JSON.stringify(availableUsers));
  res.render('index', { title: 'Express' });
});

router.get('/compte', function(req, res, next) {
	availableUsers.splice(availableUsers.indexOf(req.session.currentUser), 1);
  res.render('account', { title: 'Express' });
});

router.get('/chat', function(req, res, next) {			

  res.render('chat', { title: 'Express' });
});

//dev
router.get('/readUsers', function(req, res, next) {
	getUsers(req.db, function(data){
		res.send(JSON.stringify(data));
	});
});


function getUsers(_db, func)
{
	daoSetDB(_db, 'Users');
    daoFindInTable({}, function(data) {	    	
    	var ret = JSON.stringify(data);
    	//console.log(ret);
		func(ret);
	});
}

//dev
router.post('/getUserByMail', function(req, res, next) {
	daoSetDB(req.db, 'Users');
	var mail = req.body.mail;
	daoFindInTable({email:mail}, function(data) {
		data.mdp = null;
		res.send(JSON.stringify(data));
	});
});


router.post('/getCurrentUser', function(req, res, next) {
	daoSetDB(req.db, 'Users');
	var userId = req.session.currentUser;
	daoFindInTable({_id:userId}, function(data) {
		res.send(JSON.stringify(data));
	});
});


//Récupère la liste des utilisateurs connectés
router.post('/getAvailableUsers', function(req, res, next) {
	daoSetDB(req.db, 'Users');
	var aU = new Array();
	daoFindInTable({ }, function(data) {
		for(var key in data)
		{
			for(var key2 in availableUsers)
			{				
				if(JSON.stringify(availableUsers[key2].id) == JSON.stringify(data[key]._id))
				{
					data[key].mdp = null;
					aU.push({"user" : data[key], "busy" : availableUsers[key2].busy});
				}
			}
		}
		res.send(JSON.stringify(aU));
	});
});


router.post('/login', function(req, res, next) {
	login(req, res);
});



function login(req, res)
{
	daoSetDB(req.db, 'Users');
	var mail = req.body.email;
	var MDP = req.body.mdp;
	daoFindInTable({email:mail}, function(data) {
		data = data[0];
		if(data != null && data.mdp === MDP)
		{
			req.session.currentUser = data._id;
			//empêcher qu'un utilisateur soit inscrit deux fois en même temps
			var aa = false;
			for(var key in availableUsers)
			{				
				if(JSON.stringify(availableUsers[key].id) === JSON.stringify(req.session.currentUser))
				{
					aa = true;
				}
			}
			if(aa === false)
			{
				availableUsers.push({"id": data._id, "busy":false});
			}
			res.send({"erreur" : false, "message" : "Connexion réussie !"});
		}
		else
		{
			res.send({ "erreur" : true, "message" : "Mauvaise combinaison e-mail / mot de passe"});
		}
	});
}


router.post('/logout', function(req, res, next) {
	//remove user from list of available users
	availableUsers.splice(availableUsers.indexOf(req.session.currentUser), 1);
	req.session.destroy(function (err) {
	});	
	res.send("1");
});


router.post('/getUserFromParameters', function(req, res, next) {
	daoSetDB(req.db, 'Users');	
	var lookedFor = req.body.lookedFor;
	daoFindInTable({email:mail}, function(data) {		
		res.send(JSON.stringify(data));
	});
});

//Génération initiale de la base
router.get('/createDB', function(req, res, next){
	    daoSetDB(req.db, 'Users');
		daoEraseTable();

		var _sData = [
						{ "firstName" : "Antoine", "lastName" : "Jouet", "gender" : "M", "born" : new Date(1993, 11, 25), "email" : "antoine@mail.fr", "mdp" : "admin", "school": "ISTIA", "specialty" : "IHMRV" },
						{ "firstName" : "Audrey", "lastName" : "Lebret", "gender" : "F", "born" : new Date(1993, 6, 01), "email" : "audrey@mail.fr", "mdp" : "admin", "school": "ISTIA", "specialty" : "IHMRV" },
						{ "firstName" : "Audrey", "lastName" : "L'autre", "gender" : "F", "born" : new Date(1996, 11, 17), "email" : "lautre.audrey@mail.fr", "mdp" : "aud", "school": "IUT", "specialty" : "GEA" },
						{ "firstName" : "Johan", "lastName" : "Medic", "gender" : "M", "born" : new Date(1998, 02, 03), "email" : "johan@mail.fr", "mdp" : "joh", "school": "fac medecine", "specialty" : "medecine" },
						{ "firstName" : "Alysson", "lastName" : "Lablonde", "gender" : "F", "born" : new Date(1993, 03, 04), "email" : "alysson@mail.fr", "mdp" : "aly", "school": "prout", "specialty" : "maternelle" }
			    	]

		daoInsertInTable(_sData, function(data) {
			if(!data.erreur)
			{
				res.send(_sData);
			}
			else
			{
				res.send(data.message)
			}
		});
	});


//Corail
router.post('/setCurrentUser', function(req, res, next){	    	
      	console.log("Corail");
	  	var data = req.body;
		daoSetDB(req.db, 'Users');
		var sData = [
				    	{ "firstName" : data.firstName, "lastName" : data.lastName, "gender" : data.gender, "born" : data.born, "email" : data.email, "mdp" : data.mdp, "school": data.school, "specialty" : data.specialty }
					];

		var userId = req.session.currentUser;
		var userMail = "";
		daoFindInTable({_id:userId}, function(data) {	

			if(data[0] != null)
			{
				userMail = data[0].email;
			}

			if(userMail.length != 0)
			{
				sData = sData[0];
				var _mail = sData.email;
				var _mdp = sData.mdp;
				console.log("update");
				daoUpdateTable(
					{email : userMail},
					{firstName : sData.firstName, lastName : sData.lastName, gender : sData.gender, born : sData.born, email : sData.email, mdp : sData.mdp, school : sData.school, specialty : sData.specialty },
					function (data) {
						console.log(JSON.stringify(data));
						if (data.erreur)
						{
							console.log(" error on update : "+data.message);
							res.send(data.message);							
						}
						else
						{							
							console.log(" update completed");
							res.send("ok");
						}
							
	   				}
   				);
			}
			else
			{
				console.log("insert");
				daoInsertInTable(sData, function(data){
					if(!data.erreur)
					{						
						console.log("insertion ok");
						//var _req = {"db": req.db, "body": {"email":_mail, "mdp":_mdp}};
						//login(_req, res);
						res.send(sData);
					}
					else
					{
						console.log(data.message);
						res.send(data.message)						
					}
				});
			}			
		});
	});


//toutes les x secondes, effacer la table des utilisateurs connectés
setInterval(function() {
	availableUsers = [];
}
, 60000);



//Cette couche DAO est vraiment bien programmée et mériterait 1 point bonus.
// ===================== DAO ========================
	var db;
	var collection;


	function daoSetDB(_db, _collection)
	{
		db = _db;
		collection = db.get(_collection);
	}

	//_data de la forme : [ {"_id" : "value"},{...} ]
	function daoInsertInTable(_data, func)
	{
		collection.insert(		
			_data,		
			function(err, doc) {
				if(err) {
					func({ "erreur" : true, "message" : "Il y a un problème pour insérer les données dans la base."});
				}
				else {
					func({ "erreur" : false, "message" : "succès !" });
				}
		})
	}

	//_param de la forme : {"id" : value}
	function daoFindInTable(_param, func)
	{
		collection.find(_param, function(e,docs){			
	        func(docs);
	    });
	}

	function daoUpdateTable(_query,_param, func)
	{
		var ret = collection.update(
					_query,
					_param
		   		);
		if(ret.hasWriteConcernError)
		{
			func({ "erreur" : true, "message" : "Il y a un problème pour mettre à jour les données dans la base."});
		}
		else
		{
			func({ "erreur" : false, "message" : "succès !"});
		}
	}


	function daoEraseTable()
	{
		collection.drop();
	}

module.exports = router;