*****************************************************************
**********		  README		       **********							
*****************************************************************

- Le titre du projet : Niddlink

- La description du projet :
Nous d�veloppons un petit site de mise en relation d'�tudiants sur le campus. 
Il y a un syst�me de cr�ation de profil et de login, avec ensuite possibilit� 
de visualiser un r�sum� du profil de chaque personne connect�e (nom, pr�nom, 
�cole, sp�cialit�). On peut lancer une session de chat � deux personnes avec 
toute personne connect�e.

- Instructions de d�marrage : 
Il suffit d'avoir nodeJS install� sur le pc, de se placer � la racine du repertoire et d'ex�cuter la commande :
npm start
La base de donn�es est h�berg�e sur le web et ne n�cessite donc pas d'�tre d�marr�e.

- Membres de l'�quipes et le r�le de chacun :
	- Antoine JOUET : Partie serveur
	- Audrey LEBRET : Partie client

- Technologies utilis�es
	- Base MongoDB (h�berg�e sur MongoLab)
	- NodeJS (express)
	- JavaScript
	- Jade
	- jQuery
	- Socket.io

- lien (si possible) vers une d�monstration de l'application
	- Lien BitBucket : https://bitbucket.org/ouan49/projetweb2016_niddlink
	- Lien du site : en local (incompatibilit� d'heroku avec socket.io...)

- Bugs connus
	- Clignotement de la liste des personnes connect�es
	- Parfois non-rafraichissement de cette liste...
	- Pas de s�curit�

